package endpoints

import (
	"context"
	appRequest "git.wki.ir/idpay/backend/go-kit-test/request"
	"git.wki.ir/idpay/backend/go-kit-test/response"
	. "git.wki.ir/idpay/backend/go-kit-test/service"
	"github.com/go-kit/kit/endpoint"
)

// TodoEndpoints Endpoints collects all endpoints which compose the Todo service
type TodoEndpoints struct {
	GetAllEndPoint  endpoint.Endpoint
	GetByIDEndpoint endpoint.Endpoint
	AddEndpoint     endpoint.Endpoint
	UpdateEndpoint  endpoint.Endpoint
	DeleteEndpoint  endpoint.Endpoint
}

// 	MakeTodoEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the provided Todo
func MakeTodoEndpoints(ctx context.Context, s TodoService) TodoEndpoints {
	return TodoEndpoints{
		GetAllEndPoint:  MakeGetAllEndpoint(ctx, s),
		GetByIDEndpoint: MakeGetByIDEndpoint(ctx, s),
		AddEndpoint:     MakeAddEndpoint(ctx, s),
		UpdateEndpoint:  MakeUpdateEndpoint(ctx, s),
		DeleteEndpoint:  MakeDeleteEndpoint(ctx, s),
	}
}

func MakeGetAllEndpoint(ctx context.Context, s TodoService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		username := ctx.Value("username").(string)
		todos, err := s.GetAll(ctx, username)
		return response.GetAllUser{Todos: todos}, err
	}
}

func MakeGetByIDEndpoint(ctx context.Context, s TodoService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(appRequest.GetById)
		todo, err := s.GetById(ctx, req.ID)
		return response.GetById{Todo: todo}, err
	}
}

func MakeAddEndpoint(ctx context.Context, s TodoService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(appRequest.Add)
		todo, err := s.Add(ctx, req.Todo)
		return response.Add{Todo: todo}, err
	}
}

func MakeUpdateEndpoint(ctx context.Context, s TodoService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(appRequest.Update)
		err := s.Update(ctx, req.ID, req.Todo)
		return response.Update{}, err
	}
}

func MakeDeleteEndpoint(ctx context.Context, s TodoService) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(appRequest.Delete)
		err := s.Delete(ctx, req.ID)
		return response.Delete{}, err
	}
}
