package service

import (
	"context"
	. "git.wki.ir/idpay/backend/go-kit-test/model"
	"github.com/rs/xid"
	"math/rand"
	"sync"
	"time"
)

// TodoService for Todos
type TodoService interface {
	GetAll(ctx context.Context, username string) ([]Todo, error)
	GetById(ctx context.Context, id string) (Todo, error)
	Add(ctx context.Context, todo Todo) (Todo, error)
	Update(ctx context.Context, id string, todo Todo) error
	Delete(ctx context.Context, id string) error
}

// *** Implementation ***
type inMemoryService struct {
	sync.RWMutex
	m map[string]Todo
}

func (s *inMemoryService) GetAll(ctx context.Context, username string) ([]Todo, error) {
	s.Lock()
	defer s.Unlock()
	todos := make([]Todo, 0, len(s.m))
	for _, todo := range s.m {
		if todo.Username == username {
			todos = append(todos, todo)
		}
	}
	return todos, nil
}

// GetById Get an Todos from the database
func (s *inMemoryService) GetById(ctx context.Context, id string) (Todo, error) {
	s.Lock()
	defer s.Unlock()

	if todo, ok := s.m[id]; ok {
		return todo, nil
	}

	return Todo{}, ErrNotFound
}

// Add a Todo to memory
func (s *inMemoryService) Add(ctx context.Context, todo Todo) (Todo, error) {
	s.Lock()
	defer s.Unlock()

	todo.Id = xid.New().String()
	todo.CreatedAt = time.Now()

	s.m[todo.Id] = todo
	return todo, nil
}

// Update a Todo in memory
func (s *inMemoryService) Update(ctx context.Context, id string, todo Todo) error {
	s.Lock()
	defer s.Unlock()

	if id != todo.Id {
		return ErrInconsistentIDs
	}

	if _, ok := s.m[id]; !ok {
		return ErrNotFound
	}

	s.m[todo.Id] = todo
	return nil
}

// Delete a Todo from memory
func (s *inMemoryService) Delete(ctx context.Context, id string) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.m[id]; !ok {
		return ErrNotFound
	}

	delete(s.m, id)
	return nil
}

func NewInMemeTodoService() TodoService {
	s := &inMemoryService{
		m: map[string]Todo{},
	}
	rand.Seed(time.Now().UnixNano())
	return s
}
