package transport

import (
	"context"
	"encoding/json"
	"git.wki.ir/idpay/backend/go-kit-test/endpoints"
	. "git.wki.ir/idpay/backend/go-kit-test/model"
	"git.wki.ir/idpay/backend/go-kit-test/response"
	"git.wki.ir/idpay/backend/go-kit-test/service"
	kitendpoint "github.com/go-kit/kit/endpoint"
	kittransport "github.com/go-kit/kit/transport"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/go-kit/log"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"net/http"
)

func MakeHttpTodoHandler(ctx context.Context, service service.TodoService, logger log.Logger) http.Handler {
	router := mux.NewRouter()
	todoEndpoints := endpoints.MakeTodoEndpoints(ctx, service)

	options := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(kittransport.NewLogErrorHandler(logger)),
		kithttp.ServerErrorEncoder(encodeError),
	}

	router.Methods(http.MethodGet).Path("/todos").Handler(kithttp.NewServer(
		todoEndpoints.GetAllEndPoint,
		decodeGetRequest,
		encodeResponse,
		options...,
	))

	router.Methods(http.MethodGet).Path("/todo/{id}").Handler(kithttp.NewServer(
		todoEndpoints.GetByIDEndpoint,
		decodeGetByIdRequest,
		encodeResponse,
		options...,
	))

	router.Methods(http.MethodPost).Path("/todo").Handler(kithttp.NewServer(
		todoEndpoints.AddEndpoint,
		decodeAddTodoRequest,
		encodeResponse,
		options...,
	))

	router.Methods(http.MethodPut).Path("/todo/{id}").Handler(kithttp.NewServer(
		todoEndpoints.AddEndpoint,
		decodeAddTodoRequest,
		encodeResponse,
		options...,
	))

	router.Methods(http.MethodDelete).Path("/todo/{id}").Handler(kithttp.NewServer(
		todoEndpoints.DeleteEndpoint,
		decodeDeleteRequest,
		encodeResponse,
		options...,
	))

	return router
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(errToHTTPStatus(err))
	encodeError := json.NewEncoder(w).Encode(
		response.Errors{
			Data: []response.General{
				{
					Status:    errToHTTPStatus(err),
					Code:      errToCode(err),
					MessageEN: err.Error(),
					MessageFA: errToPersianError(err).Error(),
				},
			},
		},
	)
	if encodeError != nil {
		panic("encodeError error")
	}
}

func errToHTTPStatus(err error) int {
	switch errors.Cause(err) {
	case ErrDeadline:
		return http.StatusServiceUnavailable
	default:
		return http.StatusInternalServerError
	}
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(kitendpoint.Failer); ok && e.Failed() != nil {
		encodeError(ctx, e.Failed(), w) // A business-logic error. Thus provide those as HTTP errors.
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func errToCode(err error) int { // nolint: dupl
	switch errors.Cause(err) {
	case ErrMissingParam:
		return CodeErrMissingParam
	case ErrNotFound:
		return CodeErrInconsistentIDs
	default:
		return CodeErrUnexpected
	}
}

func errToPersianError(err error) error { // nolint: dupl
	switch errors.Cause(err) {
	case ErrMissingParam:
		return ErrPMissingParam
	case ErrNotFound:
		return ErrPNotFound
	case ErrUnexpected:
		return ErrPUnexpected
	default:
		return ErrPUnexpected
	}
}
