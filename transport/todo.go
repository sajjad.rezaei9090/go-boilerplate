package transport

import (
	"context"
	"encoding/json"
	. "git.wki.ir/idpay/backend/go-kit-test/model"
	"git.wki.ir/idpay/backend/go-kit-test/request"
	"net/http"
)

func decodeGetRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	return request.GetAllUser{}, nil
}

func decodeGetByIdRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var (
		getByIdRequest request.GetById
		err            error
	)
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	if err = d.Decode(&getByIdRequest); err != nil {
		return nil, err
	}

	if getByIdRequest.ID == "" {
		return nil, ErrMissingParam
	}
	return getByIdRequest, err
}

func decodeAddTodoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var (
		addTodoRequest request.Add
		todo           Todo
		err            error
	)
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	if err = d.Decode(&addTodoRequest); err != nil {
		return nil, err
	}

	return request.Add{Todo: todo}, err
}

func decodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var (
		updateRequest request.Update
		err           error
	)
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err = d.Decode(&updateRequest); err != nil {
		return nil, err
	}

	if updateRequest.ID == "" {
		return nil, ErrMissingParam
	}

	return updateRequest, err
}

func decodeDeleteRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var (
		deleteRequest request.Delete
		err           error
	)
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err = d.Decode(&deleteRequest); err != nil {
		return nil, err
	}

	if deleteRequest.ID == "" {
		return nil, ErrMissingParam
	}

	return deleteRequest, err
}
