package request

import . "git.wki.ir/idpay/backend/go-kit-test/model"

type (
	GetAllUser struct {
	}

	GetById struct {
		ID string `json:"id"`
	}

	Add struct {
		Todo Todo
	}

	Update struct {
		ID   string `json:"id"`
		Todo Todo
	}

	Delete struct {
		ID string `json:"id"`
	}
)
