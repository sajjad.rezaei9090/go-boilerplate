package response

import . "git.wki.ir/idpay/backend/go-kit-test/model"

//todos entity
type (
	GetAllUser struct {
		Todos []Todo `json:"todos"`
	}

	GetById struct {
		Todo Todo `json:"todo"`
	}

	Add struct {
		Todo Todo `json:"todo"`
	}

	Update struct {
	}

	Delete struct {
	}
)

//general entity
type (
	Meta struct {
		CurrentPage int `json:"current_page"`
		PerPage     int `json:"per_page"`
		LastPage    int `json:"last_page"`
		From        int `json:"from"`
		To          int `json:"to"`
		Total       int `json:"total"`
	}

	General struct {
		MessageEN string `json:"detail"`
		MessageFA string `json:"detail_locale"`
		Status    int    `json:"status"`
		Code      int    `json:"code"`
	}

	Errors struct {
		Data []General `json:"errors"`
	}

	Version struct {
		Tag     string `json:"tag"`
		Commit  string `json:"commit"`
		Date    string `json:"date"`
		Service string `json:"service"`
	}
)
