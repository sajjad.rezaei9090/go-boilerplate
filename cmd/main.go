package main

import (
	"context"
	"fmt"
	s "git.wki.ir/idpay/backend/go-kit-test/service"
	"git.wki.ir/idpay/backend/go-kit-test/transport"
	"github.com/go-kit/log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {

	ctx := context.Background()
	service := s.NewInMemeTodoService()
	addr := "8000"

	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "listen", addr, "caller", log.DefaultCaller)

	handler := transport.MakeHttpTodoHandler(ctx, service, logger)

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		logger.Log("transport", "HTTP", "address", addr)
		errs <- http.ListenAndServe(addr, handler)
	}()

	err := logger.Log("exit", <-errs)
	if err != nil {
		return
	}
}
