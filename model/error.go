package model

import "github.com/pkg/errors"

var (
	ErrUnexpected      = errors.New("unexpected error")
	ErrDeadline        = errors.New("remote service is temporary unavailable")
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrNotFound        = errors.New("not found")
	ErrMissingParam    = errors.New("missing params")

	ErrPUnexpected   = errors.New("‌خطای غیر منتظره")
	ErrPNotFound     = errors.New("داده ای با این شناسه یافت نشد")
	ErrPMissingParam = errors.New("")
)

const (
	CodeSuccess = iota

	CodeErrUnexpected
	CodeErrInconsistentIDs
	CodeErrMissingParam
)
