package model

import "time"

type Todo struct {
	Id        string    `json:"id"`
	Username  string    `json:"username"`
	Text      string    `json:"text"`
	Completed bool      `json:"completed"`
	CreatedAt time.Time `json:"created_at"`
}
