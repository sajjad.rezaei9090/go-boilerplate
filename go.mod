module git.wki.ir/idpay/backend/go-kit-test

go 1.17

require (
	github.com/go-kit/kit v0.12.0
	github.com/go-kit/log v0.2.0
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/rs/xid v1.4.0
)

require github.com/go-logfmt/logfmt v0.5.1 // indirect
